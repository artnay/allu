package fi.hel.allu.model.service;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import fi.hel.allu.common.domain.types.ApplicationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fi.hel.allu.common.domain.types.ApplicationTagType;
import fi.hel.allu.common.domain.types.StatusType;
import fi.hel.allu.common.types.CommentType;
import fi.hel.allu.common.util.ApplicationIdUtil;
import fi.hel.allu.model.dao.*;
import fi.hel.allu.model.domain.*;

import static fi.hel.allu.common.domain.types.ApplicationTagType.*;

/**
 * Service for replacing application (korvaava päätös).
 */
@Service
public class ApplicationReplacementService {
  private static final Set<CommentType> COMMENT_TYPES_NOT_COPIED = new HashSet<>(Arrays.asList(CommentType.PROPOSE_APPROVAL,
      CommentType.PROPOSE_REJECT, CommentType.PROPOSE_TERMINATION));

  private static final Set<ApplicationTagType> TAG_TYPES_NOT_COPIED = new HashSet<>(Arrays.asList(
      // If SAP ID missing also from replacing application,
      // this is generated when its' moved to decision state
      SAP_ID_MISSING,
      // Open supervision tasks not copied to replacing application
      // so don't copy supervision task related tags either.
      PRELIMINARY_SUPERVISION_REQUESTED,
      PRELIMINARY_SUPERVISION_REJECTED,
      PRELIMINARY_SUPERVISION_DONE,
      SUPERVISION_REQUESTED,
      SUPERVISION_REJECTED,
      SUPERVISION_DONE,
      OPERATIONAL_CONDITION_REPORTED
  ));

  private static final Set<StatusType> REPLACEMENT_IS_ALLOWED = new HashSet<>(Arrays.asList(
      StatusType.DECISION,
      StatusType.OPERATIONAL_CONDITION
  ));


  private final ApplicationService applicationService;
  private final ApplicationDao applicationDao;
  private final CommentDao commentDao;
  private final LocationService locationService;
  private final DepositDao depositDao;
  private final SupervisionTaskDao supervisionTaskDao;
  private final ChargeBasisDao chargeBasisDao;
  private final InvoiceDao invoiceDao;

  @Autowired
  public ApplicationReplacementService(ApplicationService applicationService, ApplicationDao applicationDao, CommentDao commentDao,
      LocationService locationService, DepositDao depositDao, SupervisionTaskDao supervisionTaskDao, ChargeBasisDao chargeBasisDao,
      InvoiceDao invoiceDao) {
    this.applicationService = applicationService;
    this.locationService = locationService;
    this.applicationDao = applicationDao;
    this.commentDao = commentDao;
    this.depositDao = depositDao;
    this.supervisionTaskDao = supervisionTaskDao;
    this.chargeBasisDao = chargeBasisDao;
    this.invoiceDao = invoiceDao;
  }

  /**
   * Replace application with given ID.
   * <ul>
   * <li>Creates a copy from application with given ID</li>
   * <li>Replaced application must be in {@link StatusType#DECISION}-state</li>
   * <li>Sets original application to {@link StatusType#REPLACED}-state and new application to {@link StatusType#HANDLING}-state</li>
   * <li>Removes original application from project</li>
   * <li>Sets decision fields (decision maker and decision time) of replacing application to null
   * <li>Copies following data from original application to replacing application
   *  <ul>
   *  <li>{@link Location}</li>
   *  <li>{@link Comment}</li>
   *  <li>{@link ApplicationTag}</li>
   *  <li>{@link SupervisionTask}</li>
   *  <li>{@link Deposit}</li>
   *  <li>{@link AttachmentInfo}</li>
   *  </ul>
   * </li>
   * </ul>
   *
   * @param applicationId Id of the application to replace
   * @return ID of the replacing application.
   */
  @Transactional
  public int replaceApplication(int applicationId, int userId) {
    // Copy application
    Application applicationToReplace = applicationService.findById(applicationId);
    Application replacingApplication = addReplacingApplication(applicationToReplace, userId);

    copyApplicationRelatedData(applicationId, replacingApplication, userId);

    // Update application status
    updateStatus(replacingApplication);
    // Remove replaced application from project
    applicationDao.updateProject(null, Collections.singletonList(applicationToReplace.getId()));
    // Set replaces and replaced by
    applicationDao.setApplicationReplaced(applicationId, replacingApplication.getId());
    return replacingApplication.getId();
  }

  private void copyApplicationRelatedData(int applicationId, Application replacingApplication, int userId) {
    commentDao.copyApplicationComments(applicationId, replacingApplication.getId(), COMMENT_TYPES_NOT_COPIED);
    applicationDao.copyApplicationAttachments(applicationId, replacingApplication.getId());
    depositDao.copyApplicationDeposit(applicationId, replacingApplication.getId());
    supervisionTaskDao.copyApprovedSupervisionTasks(applicationId, replacingApplication.getId());
    chargeBasisDao.copyManualChargeBasisEntries(applicationId, replacingApplication.getId(), invoiceDao.getInvoicedChargeBasisIds(applicationId));
  }

  private Application addReplacingApplication(Application applicationToReplace, int userId) {
    validateReplacementAllowed(applicationToReplace);
    Application replacingApplication = createReplacingApplication(applicationToReplace);
    replacingApplication = applicationService.insert(replacingApplication, userId);
    return replacingApplication;
  }

  private void validateReplacementAllowed(Application applicationToReplace) {
    final boolean replacementAllowedForStatus = REPLACEMENT_IS_ALLOWED.contains(applicationToReplace.getStatus());
    final boolean isReplaced = applicationToReplace.getReplacedByApplicationId() != null;
    if (!replacementAllowedForStatus || isReplaced) {
      throw new IllegalArgumentException("Application already replaced or in invalid state, replacement not allowed");
    }
  }

  private Application createReplacingApplication(Application applicationToReplace) {
    Application application = new Application();
    application.setApplicationId(generateReplacingApplicationId(applicationToReplace));
    application.setCustomersWithContacts(applicationToReplace.getCustomersWithContacts());
    application.setDecisionPublicityType(applicationToReplace.getDecisionPublicityType());
    application.setEndTime(applicationToReplace.getEndTime());
    application.setExtension(applicationToReplace.getExtension());
    application.setOwner(applicationToReplace.getOwner());
    application.setInvoiceRecipientId(applicationToReplace.getInvoiceRecipientId());
    application.setName(applicationToReplace.getName());
    application.setNotBillable(applicationToReplace.getNotBillable());
    application.setNotBillableReason(applicationToReplace.getNotBillableReason());
    application.setProjectId(applicationToReplace.getProjectId());
    application.setRecurringEndTime(applicationToReplace.getRecurringEndTime());
    application.setStartTime(applicationToReplace.getStartTime());
    application.setType(applicationToReplace.getType());
    application.setCustomerReference(applicationToReplace.getCustomerReference());
    application.setInvoicingDate(applicationToReplace.getInvoicingDate());
    application.setIdentificationNumber(applicationToReplace.getIdentificationNumber());
    setApplicationLocations(applicationToReplace, application);
    // Application DAO will automatically create copies of following
    application.setApplicationTags(applicationToReplace.getApplicationTags().stream()
        .filter(t -> !TAG_TYPES_NOT_COPIED.contains(t.getType())).collect(Collectors.toList()));
    application.getApplicationTags().forEach(t -> t.setCreationTime(ZonedDateTime.now()));
    application.setDecisionDistributionList(applicationToReplace.getDecisionDistributionList());
    application.setKindsWithSpecifiers(applicationToReplace.getKindsWithSpecifiers());
    application.setReplacesApplicationId(applicationToReplace.getId());
    application.setExternalOwnerId(applicationToReplace.getExternalOwnerId());
    application.setExternalApplicationId(applicationToReplace.getExternalApplicationId());
    application.setSkipPriceCalculation(applicationToReplace.getSkipPriceCalculation());
    return application;
  }

  public void setApplicationLocations(Application applicationToReplace, Application application) {
    List<Location> locations = locationService.findByApplicationId(applicationToReplace.getId());
    locations.forEach(l -> clearIds(l));
    application.setLocations(locations);
  }

  private void clearIds(Location location) {
    location.setId(null);
    location.setApplicationId(null);
  }

  private String generateReplacingApplicationId(Application applicationToReplace) {
    String applicationId = applicationToReplace.getApplicationId();

    final List<ApplicationIdentifier> appIds = applicationDao.findByApplicationIdStartingWith(
            ApplicationIdUtil.getBaseApplicationId(applicationId));
    if (!appIds.isEmpty()) {
      // Find latest application ID
      Collections.sort(appIds, (a1, a2) -> Integer.valueOf(a2.getId()).compareTo(a1.getId()));
      applicationId = appIds.get(0).getApplicationId();
    }
    return ApplicationIdUtil.generateReplacingApplicationId(applicationId);
  }

  private void updateStatus(Application replacingApplication) {
    applicationDao.updateStatus(replacingApplication.getId(), StatusType.HANDLING);

    if (ApplicationType.CABLE_REPORT == replacingApplication.getType()) {
      applicationDao.setTargetState(replacingApplication.getId(), StatusType.DECISION);
    }
  }
}

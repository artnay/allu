package fi.hel.allu.common.types;

public enum ChargeBasisType {
  CALCULATED,
  NEGLIGENCE_FEE,
  ADDITIONAL_FEE,
  DISCOUNT
}

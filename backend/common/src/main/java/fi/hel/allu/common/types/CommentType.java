package fi.hel.allu.common.types;

/**
 * Valid comment types for application comments
 */
public enum CommentType {
  INVOICING,
  RETURN,
  REJECT,
  INTERNAL,
  PROPOSE_APPROVAL,
  PROPOSE_REJECT,
  OPERATIONAL_CONDITION_ACCEPTED,
  OPERATIONAL_CONDITION_REJECTED,
  FINAL_SUPERVISION_ACCEPTED,
  FINAL_SUPERVISION_REJECTED
}

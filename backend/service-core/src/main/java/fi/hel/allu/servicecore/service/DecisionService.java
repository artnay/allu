package fi.hel.allu.servicecore.service;

import fi.hel.allu.common.domain.types.ApplicationKind;
import fi.hel.allu.common.domain.types.ApplicationType;
import fi.hel.allu.common.domain.types.ChargeBasisUnit;
import fi.hel.allu.common.domain.types.CustomerRoleType;
import fi.hel.allu.common.types.DefaultTextType;
import fi.hel.allu.common.types.EventNature;
import fi.hel.allu.model.domain.ChargeBasisEntry;
import fi.hel.allu.pdf.domain.CableInfoTexts;
import fi.hel.allu.pdf.domain.ChargeInfoTexts;
import fi.hel.allu.pdf.domain.DecisionJson;
import fi.hel.allu.servicecore.config.ApplicationProperties;
import fi.hel.allu.servicecore.domain.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DecisionService {

  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(DecisionService.class);

  private static final FixedLocationJson BAD_LOCATION;
  private static final String ADDRESS_LINE_SEPARATOR = "; ";
  private static final Map<DefaultTextType, String> defaultTextTypeTranslations;
  private static final Map<ApplicationKind, String> applicationKindTranslations;

  private static final String UNKNOWN_ADDRESS = "[Osoite ei tiedossa]";

  private final ApplicationProperties applicationProperties;
  private final RestTemplate restTemplate;
  private final LocationService locationService;
  private final CustomerService customerService;
  private final ContactService contactService;
  private final ApplicationServiceComposer applicationServiceComposer;
  private final ChargeBasisService chargeBasisService;
  private final ZoneId zoneId;
  private final Locale locale;
  private final DateTimeFormatter dateTimeFormatter;
  private final DateTimeFormatter timeStampFormatter;
  private final DecimalFormat decimalFormat;
  private final NumberFormat currencyFormat;

  static {
    BAD_LOCATION = new FixedLocationJson();
    BAD_LOCATION.setArea("Tuntematon alue");
    Map<DefaultTextType, String> tempMap = new HashMap<>();
    tempMap.put(DefaultTextType.TELECOMMUNICATION, "Tietoliikenne");
    tempMap.put(DefaultTextType.ELECTRICITY, "Sähkö");
    tempMap.put(DefaultTextType.WATER_AND_SEWAGE, "Vesi ja viemäri");
    tempMap.put(DefaultTextType.DISTRICT_HEATING_COOLING, "Kaukolämpö/jäähdytys");
    tempMap.put(DefaultTextType.GAS, "Kaasu");
    tempMap.put(DefaultTextType.UNDERGROUND_STRUCTURE, "Maanalainen rakenne/tila");
    tempMap.put(DefaultTextType.TRAMWAY, "Raitiotie");
    tempMap.put(DefaultTextType.STREET_HEATING, "Katulämmitys");
    tempMap.put(DefaultTextType.SEWAGE_PIPE, "Jäteputki");
    tempMap.put(DefaultTextType.GEOTHERMAL_WELL, "Maalämpökaivo");
    tempMap.put(DefaultTextType.GEOTECHNICAL_OBSERVATION_POST, "Geotekninen tarkkailupiste");
    tempMap.put(DefaultTextType.OTHER, "Yleisesti/muut");
    defaultTextTypeTranslations = Collections.unmodifiableMap(tempMap);
    applicationKindTranslations = Collections.unmodifiableMap(createApplicationKindTranslations());
  }

  @Autowired
  public DecisionService(
      ApplicationProperties applicationProperties,
      RestTemplate restTemplate,
      LocationService locationService,
      ApplicationServiceComposer applicationServiceComposer,
      CustomerService customerService,
      ContactService contactService, ChargeBasisService chargeBasisService) {
    this.applicationProperties = applicationProperties;
    this.restTemplate = restTemplate;
    this.locationService = locationService;
    this.applicationServiceComposer = applicationServiceComposer;
    this.customerService = customerService;
    this.contactService = contactService;
    this.chargeBasisService = chargeBasisService;
    zoneId = ZoneId.of("Europe/Helsinki");
    locale = new Locale("fi", "FI");
    dateTimeFormatter = DateTimeFormatter.ofPattern("d.M.uuuu");
    timeStampFormatter = DateTimeFormatter.ofPattern("d.M.uuuu 'kello' HH.mm");
    decimalFormat = new DecimalFormat("0.##");
    currencyFormat = NumberFormat.getCurrencyInstance(locale);
  }

  /*
   * Set up translations for application kinds (FIXME: get them from MetaService
   * instead)
   */
  private static Map<ApplicationKind, String> createApplicationKindTranslations() {
    Map<ApplicationKind, String> translations = new HashMap<>();
    translations.put(ApplicationKind.BRIDGE_BANNER, "Banderollit silloissa");
    translations.put(ApplicationKind.BENJI, "Benji-hyppylaite");
    translations.put(ApplicationKind.PROMOTION_OR_SALES, "Esittely- tai myyntitila liikkeen edustalla");
    translations.put(ApplicationKind.URBAN_FARMING, "Kaupunkiviljelypaikka");
    translations.put(ApplicationKind.KESKUSKATU_SALES, "Keskuskadun myyntipaikka");
    translations.put(ApplicationKind.SUMMER_THEATER, "Kesäteatterit");
    translations.put(ApplicationKind.DOG_TRAINING_FIELD, "Koirakoulutuskentät");
    translations.put(ApplicationKind.DOG_TRAINING_EVENT, "Koirakoulutustapahtuma");
    translations.put(ApplicationKind.SMALL_ART_AND_CULTURE, "Pienimuotoinen taide- ja kulttuuritoiminta");
    translations.put(ApplicationKind.SEASON_SALE, "Sesonkimyynti");
    translations.put(ApplicationKind.CIRCUS, "Sirkus/tivolivierailu");
    translations.put(ApplicationKind.ART, "Taideteos");
    translations.put(ApplicationKind.STORAGE_AREA, "Varastoalue");
    translations.put(ApplicationKind.OTHER, "Muu");
    return translations;
  }

  /**
   * Generate the decision PDF for given application and save it to model
   * service
   *
   * @param applicationId
   *          the application's ID
   * @throws IOException
   *           when model-service responds with error
   */
  public void generateDecision(int applicationId, ApplicationJson application) throws IOException {
    DecisionJson decisionJson = new DecisionJson();
    fillJson(decisionJson, application);
    byte[] pdfData = restTemplate.postForObject(
        applicationProperties.getGeneratePdfUrl(), decisionJson, byte[].class,
        styleSheetName(application));
    // Store the generated PDF to model:
    MultiValueMap<String, Object> requestParts = new LinkedMultiValueMap<>();
    requestParts.add("file", new ByteArrayResource(pdfData) {
      @Override // return some filename so that Spring handles this as file
      public String getFilename() {
        return "file.pdf";
      }
    });
    HttpHeaders requestHeader = new HttpHeaders();
    requestHeader.setContentType(MediaType.MULTIPART_FORM_DATA);
    HttpEntity<?> requestEntity = new HttpEntity<>(requestParts, requestHeader);
    // ...then execute the request
    ResponseEntity<String> response = restTemplate.exchange(
        applicationProperties.getStoreDecisionUrl(), HttpMethod.POST,
        requestEntity, String.class, applicationId);
    if (!response.getStatusCode().is2xxSuccessful()) {
      throw new IOException(response.getBody());
    }
  }

  /**
   * Get the decision PDF for given application from the model service
   *
   * @param applicationId
   *          the application's ID
   * @return PDF data
   */
  public byte[] getDecision(int applicationId) {
    return restTemplate.getForObject(
        applicationProperties.getDecisionUrl(), byte[].class,
        applicationId);
  }

  /**
   * Get the decision preview PDF for given application from the model service
   *
   * @param application the application data whose PDF preview is created.
   * @return PDF data
   */
  public byte[] getDecisionPreview(ApplicationJson application) {
    DecisionJson decisionJson = new DecisionJson();
    fillJson(decisionJson, application);
    decisionJson.setDraft(true);
    return restTemplate.postForObject(applicationProperties.getGeneratePdfUrl(),
        decisionJson, byte[].class, styleSheetName(application));
  }

  private void fillJson(DecisionJson decisionJson, ApplicationJson application) {
    decisionJson.setEventName(application.getName());
    decisionJson.setDecisionId(application.getApplicationId());
    decisionJson.setCustomerAddressLines(customerAddressLines(application));
    decisionJson.setCustomerContactLines(customerContactLines(application));
    decisionJson.setSiteAddressLine(siteAddressLine(application));
    if (application.getLocations() != null) {
      decisionJson.setSiteArea(String.format("%.0f", Math.ceil(application.getLocations().stream().mapToDouble(l -> l.getArea()).sum())));
    }
    if (application.getType() == null) {
      throw new IllegalArgumentException("Application type is required");
    }
    switch (application.getType()) {
    case EVENT:
      fillEventSpecifics(decisionJson, application);
      break;
    case SHORT_TERM_RENTAL:
        fillShortTermRentalSpecifics(decisionJson, application);
      break;
    case CABLE_REPORT:
      fillCableReportSpecifics(decisionJson, application);
      break;
    default:
      break;
    }
    UserJson handler = application.getHandler();
    if (handler != null) {
      decisionJson.setHandlerTitle(handler.getTitle());
      decisionJson.setHandlerName(handler.getRealName());
    }
    List<AttachmentInfoJson> attachments = application.getAttachmentList();
    if (attachments != null) {
      decisionJson.setAttachmentNames(attachments.stream().map(a -> a.getDescription()).collect(Collectors.toList()));
    }
    decisionJson.setReservationStartDate(formatDateWithDelta(application.getStartTime(), 0));
    decisionJson.setReservationEndDate(formatDateWithDelta(application.getEndTime(), 0));
    decisionJson.setNumReservationDays(daysBetween(application.getStartTime(), application.getEndTime()) + 1);
    String additionalInfos = String.join("; ",
        streamFor(application.getLocations()).map(LocationJson::getAdditionalInfo).filter(p -> p != null)
            .map(p -> p.trim()).filter(p -> !p.isEmpty()).collect(Collectors.toList()));
    decisionJson.setSiteAdditionalInfo(additionalInfos);
    decisionJson.setDecisionDate(
        Optional.ofNullable(application.getDecisionTime()).map(dt -> formatDateWithDelta(dt, 0)).orElse("[Päätöspvm]"));
    decisionJson.setVatPercentage(24); // FIXME: find actual value somehow
    decisionJson.setAdditionalConditions(
        splitToList(Optional.ofNullable(application.getExtension()).map(e -> e.getTerms())));
    decisionJson.setDecisionTimestamp(ZonedDateTime.now().withZoneSameInstant(zoneId).format(timeStampFormatter));
    UserJson decider = application.getDecisionMaker();
    if (decider != null) {
      decisionJson.setDeciderTitle(decider.getTitle());
      decisionJson.setDeciderName(decider.getRealName());
    }
    decisionJson.setAppealInstructions("[Muutoksenhakuohjeet]");
    decisionJson.setNotBillable(Boolean.TRUE.equals(application.getNotBillable()));
    decisionJson.setNotBillableReason(application.getNotBillableReason());
    Integer priceInCents = application.getCalculatedPrice();
    if (priceInCents != null) {
      decisionJson.setTotalRent(currencyFormat.format(priceInCents / 100.0));
      decisionJson.setSeparateBill(priceInCents > 0);
    }
    fillCargeBasisInfo(decisionJson, application);
  }

  /*
   * Split the given string into a list of strings. For empty Optional, give
   * empty list.
   */
  private List<String> splitToList(Optional<String> string) {
    return string.map(s -> s.split("\n"))
        .map(a -> Arrays.stream(a)).map(s -> s.collect(Collectors.toList()))
        .orElse(Collections.emptyList());
  }
  /*
   * Read application's charge basis entries, order them, and generate matching
   * charge info texts.
   */
  private void fillCargeBasisInfo(DecisionJson decisionJson, ApplicationJson application) {
    if (application.getId() == null) {
      return;
    }
    List<ChargeBasisEntry> chargeBasisEntries = chargeBasisService.getChargeBasis(application.getId());
    Map<String, List<ChargeBasisEntry>> entriesByReferred = chargeBasisEntries.stream()
        .collect(Collectors.groupingBy(cbe -> StringUtils.defaultString(cbe.getReferredTag())));
    List<Pair<Integer, ChargeBasisEntry>> orderedEntries = listReferringEntries(entriesByReferred, "", 0);
    decisionJson.setChargeInfoEntries(orderedEntries.stream().map(p -> new ChargeInfoTexts(p.getLeft(),
        p.getRight().getText(), p.getRight().getExplanation(),
        chargeQuantity(p.getRight()), chargeUnitPrice(p.getRight()), chargeNetPrice(p.getRight())))
        .collect(Collectors.toList()));
  }

  /*
   * What to write in the "quantity" column in charge itemization? (skipped for
   * percent entries)
   */
  private String chargeQuantity(ChargeBasisEntry e) {
    if (ChargeBasisUnit.PERCENT.equals(e.getUnit())) {
      return null;
    } else {
      return decimalFormat.format(e.getQuantity()) + unitString(e.getUnit());
    }
  }

  /*
   * How to write the charge unit in itemization
   */
  private String unitString(ChargeBasisUnit unit) {
    switch (unit) {
      case DAY:
        return " pv";
      case HOUR:
        return " t";
      case MONTH:
        return " kk";
      case PERCENT:
        return " %";
      case PIECE:
        return " kpl";
      case SQUARE_METER:
        return " m²";
      case WEEK:
        return " vko";
      case YEAR:
        return " v";
      default:
        return "";
    }
  }

  /*
   * How to write the unit price in itemization (skipped for percentage entries)
   */
  private String chargeUnitPrice(ChargeBasisEntry e) {
    if (ChargeBasisUnit.PERCENT.equals(e.getUnit())) {
      return null;
    } else {
      return "à " + currencyFormat.format(e.getUnitPrice() * 0.01);
    }
  }

  /*
   * How to write the total price in itemization
   */
  private String chargeNetPrice(ChargeBasisEntry e) {
    if (ChargeBasisUnit.PERCENT.equals(e.getUnit())) {
      return e.getQuantity() + " %";
    } else {
      return currencyFormat.format(e.getNetPrice() * 0.01);
    }
  }

  /*
   * Recursively go trough the multimap of [key, referring entries] to generate
   * the list of entries in referral order:
   *
   * EntryA
   * +--EntryB(refers A)
   *     +--- EntryD(refers B)
   * +--EntryC(refers A)
   * EntryE
   */
  private List<Pair<Integer, ChargeBasisEntry>> listReferringEntries(
      Map<String, List<ChargeBasisEntry>> entriesByReferred, String key,
      int level) {
    // Avoid infinite recursion if data has errors:
    final int RECURSION_LIMIT = 99;
    if (level > RECURSION_LIMIT) {
      return Collections.emptyList();
    }
    List<Pair<Integer, ChargeBasisEntry>> result = new ArrayList<>();
    entriesByReferred.getOrDefault(key, Collections.emptyList()).forEach(e -> {
      result.add(Pair.of(level, e));
      if (!StringUtils.isEmpty(e.getTag())) {
        result.addAll(listReferringEntries(entriesByReferred, e.getTag(), level + 1));
      }
    });
    return result;
  }

  private void fillShortTermRentalSpecifics(DecisionJson decisionJson, ApplicationJson application) {
    ShortTermRentalJson strj = (ShortTermRentalJson) application.getExtension();
    if (strj != null) {
      decisionJson
          .setEventNature("Lyhytaikainen maanvuokraus, " + applicationKindTranslations.get(application.getKind()));
      decisionJson.setEventDescription(strj.getDescription());
      decisionJson.setPriceBasisText(shortTermPriceBasis(application.getKind()));
    }
    if (ApplicationKind.BRIDGE_BANNER.equals(application.getKind())) {
      // For bridge banners, site area should be skipped in printout
      decisionJson.setSiteArea(null);
    }
  }

  /*
   * Return the application-kind specific price basis text for Short term
   * rentals
   */
  private String shortTermPriceBasis(ApplicationKind kind) {
    switch (kind) {
      case BENJI:
        return "320 &euro;/p&auml;iv&auml; + alv";
      case BRIDGE_BANNER:
        return "<ul><li>Ei-kaupalliset toimijat: 150 &euro;/kalenteriviikko + alv</li>"
            + "<li>Kaupalliset toimijat: 750 &euro;/kalenteriviikko + alv</li></ul>";
      case CIRCUS:
        return "200 €/p&auml;iv&auml; + alv";
      case DOG_TRAINING_EVENT:
        return "<ul><li>Kertamaksu yhdistyksille: 50 &euro;/kerta + alv</li>"
            + "<li>Kertamaksu yrityksille: 100 &euro;/kerta + alv</li></ul>";
      case DOG_TRAINING_FIELD:
        return "<ul><li>Vuosivuokra yhdistyksille: 100 &euro;/vuosi</li>"
            + "<li>Vuosivuokra yrityksille: 200 &euro;/vuosi</li></ul>";
      case KESKUSKATU_SALES:
      case SEASON_SALE:
        return "<ul><li>(1&ndash;14 p&auml;iv&auml;&auml;): 50 &euro;/p&auml;iv&auml;/alkava 10 m&sup2; + alv</li>"
            + "<li>(15. p&auml;iv&auml;st&auml; alkaen): 25 &euro;/p&auml;iv&auml;/alkava 10 m&sup2; + alv</li></ul>";
      case PROMOTION_OR_SALES:
        return "150 &euro;/kalenterivuosi + alv";
      case SUMMER_THEATER:
        return "120 &euro;/kuukausi n&auml;yt&auml;nt&ouml;ajalta";
      case URBAN_FARMING:
        return "2 &euro;/m&sup2;/viljelykausi";
      case OTHER:
      case ART:
      case SMALL_ART_AND_CULTURE:
      case STORAGE_AREA:
        return null;
      default:
        return "[FIXME: Perustetta ei m&auml;&auml;ritetty]";
    }
  }

  private void fillEventSpecifics(DecisionJson decisionJson, ApplicationJson application) {
    EventJson ej = (EventJson) application.getExtension();
    if (ej != null) {
      decisionJson.setEventStartDate(formatDateWithDelta(ej.getEventStartTime(), 0));
      decisionJson.setEventEndDate(formatDateWithDelta(ej.getEventEndTime(), 0));
      decisionJson.setNumEventDays(daysBetween(ej.getEventStartTime(), ej.getEventEndTime()) + 1);
      decisionJson.setBuildStartDate(formatDateWithDelta(buildTearDate(application.getStartTime(), ej.getEventStartTime()), 0));
      decisionJson.setBuildEndDate(formatDateWithDelta(ej.getEventStartTime(), -1));
      decisionJson.setTeardownStartDate(formatDateWithDelta(ej.getEventEndTime(), 1));
      decisionJson.setTeardownEndDate(formatDateWithDelta(buildTearDate(application.getEndTime(), ej.getEventEndTime()), 0));

      decisionJson.setNumBuildAndTeardownDays(
          daysBetween(application.getStartTime(), ej.getEventStartTime())
          + daysBetween(ej.getEventEndTime(), application.getEndTime()));
      decisionJson.setReservationTimeExceptions(ej.getTimeExceptions());
      decisionJson.setEventDescription(ej.getDescription());
      decisionJson.setStructureArea(String.format("%.0f", ej.getStructureArea()));
      decisionJson.setStructureDescription(ej.getStructureDescription());
      decisionJson.setEventUrl(ej.getUrl());
      decisionJson.setHasEkokompassi(ej.isEcoCompass());
      decisionJson.setEventNature(eventNature(ej.getNature()));
    }
  }

  private ZonedDateTime buildTearDate(ZonedDateTime applicationDate, ZonedDateTime eventDate) {
    if (applicationDate != null && eventDate != null) {
      // build and tear dates exist when application's date differs from event's date
      return applicationDate.equals(eventDate) ? null : applicationDate;
    }
    return null;
  }

  private void fillCableReportSpecifics(DecisionJson decisionJson, ApplicationJson applicationJson) {
    CableReportJson cableReportJson = (CableReportJson) applicationJson.getExtension();
    if (cableReportJson != null) {
      cableReportJson.setValidityTime(ZonedDateTime.now().plusMonths(1));
      decisionJson.setCableReportValidUntil(formatDateWithDelta(cableReportJson.getValidityTime(), 0));
      decisionJson.setWorkDescription(cableReportJson.getWorkDescription());
      applicationServiceComposer.updateApplication(applicationJson.getId(), applicationJson);
      decisionJson.setCableInfoEntries(cableReportJson.getInfoEntries().stream()
          .map(i -> new CableInfoTexts(defaultTextTypeTranslations.get(i.getType()), i.getAdditionalInfo()))
          .collect(Collectors.toList()));
      decisionJson.setMapExtractCount(Optional.ofNullable(cableReportJson.getMapExtractCount()).orElse(0));
    }
    // Override customer contact & address lines
    Optional<Pair<CustomerJson, ContactJson>> orderer = cableReportOrderer(applicationJson);

    decisionJson.setCustomerContactLines(cableReportContactLines(orderer));
    decisionJson.setCableReportOrderer(
        orderer.map(e -> e.getValue().getName()).orElse("[Johtoselvityksen tilaajan nimi puuttuu]"));
    decisionJson.setCustomerAddressLines(cableReportAddressLines(applicationJson));
  }

  /*
   * Helper to create streams for possibly null collections
   */
  private static <T> Stream<T> streamFor(Collection<T> coll) {
    return Optional.ofNullable(coll).orElse(Collections.emptyList()).stream();
  }

  /* Find the customer and contact that ordered the application */
  private Optional<Pair<CustomerJson, ContactJson>> cableReportOrderer(
      ApplicationJson applicationJson) {
    CableReportJson cableReport = (CableReportJson)applicationJson.getExtension();
    return Optional.ofNullable(cableReport.getOrderer())
        .map(id -> contactService.findById(id))
        .map(contact -> Pair.of(customerService.findCustomerById(contact.getCustomerId()), contact));
  }
  /*
   * Find the customer and contact that left the cable report and return them
   */
  private List<String> cableReportContactLines(Optional<Pair<CustomerJson, ContactJson>> orderer) {
    if (!orderer.isPresent()) {
      return Collections.singletonList("[Tilaajatieto puuttuu]");
    }
    final CustomerJson customer = orderer.get().getKey();
    final ContactJson contact = orderer.get().getValue();
    return Arrays.asList(
        customer.getName(), contact.getName(), contact.getPhone(), contact.getEmail())
        .stream().filter(p -> p != null && !p.trim().isEmpty()).collect(Collectors.toList());
  }

  /*
   * For cable reports, the customer address data should come from the customer
   * that is doing the work
   */
  private List<String> cableReportAddressLines(ApplicationJson applicationJson) {
    CustomerWithContactsJson contractor = streamFor(applicationJson.getCustomersWithContacts())
        .filter(cwc -> CustomerRoleType.CONTRACTOR.equals(cwc.getRoleType())).findFirst().orElse(null);
    if (contractor == null) {
      return Collections.singletonList("[Kaivajan tiedot puuttuvat]");
    }
    final CustomerJson customer = contractor.getCustomer();
    return Arrays
        .asList(customer.getName(), customer.getPostalAddress().getStreetAddress(),
            customer.getPostalAddress().getCity(), customer.getPhone())
        .stream().filter(p -> p != null && !p.trim().isEmpty()).collect(Collectors.toList());

  }
  private String formatDateWithDelta(ZonedDateTime zonedDateTime, int deltaDays) {
    if (zonedDateTime == null) {
      return null;
    }
    return zonedDateTime.plusDays(deltaDays).withZoneSameInstant(zoneId)
        .format(dateTimeFormatter);
  }

  private List<String> customerAddressLines(ApplicationJson applicationJson) {
    // return lines in format {"[Customer name], [SSID]", "[address, Postal
    // code + city]",
    // "[email, phone]"}
    // TODO: perhaps this should work with other than APPLICANT roles too?
    Optional<CustomerWithContactsJson> cwcOpt =
        applicationJson.getCustomersWithContacts().stream().filter(cwc -> CustomerRoleType.APPLICANT.equals(cwc.getRoleType())).findFirst();

    final List<String> addressLines = new ArrayList<>();
    cwcOpt.ifPresent(cwc -> {
      addressLines.addAll(
          Arrays.asList(
              String.format("%s, %s", cwc.getCustomer().getName(), cwc.getCustomer().getRegistryKey()),
              postalAddress(cwc.getCustomer().getPostalAddress()),
              String.format("%s, %s", cwc.getCustomer().getEmail(), cwc.getCustomer().getPhone())));
    });
    return addressLines;
  }

  private List<String> customerContactLines(ApplicationJson application) {
    // returns {"[Yhteyshenkilön nimi]", "[Sähköpostiosoite, puhelin]"}

    Optional<CustomerWithContactsJson> cwcOpt =
        application.getCustomersWithContacts().stream().filter(cwc -> CustomerRoleType.APPLICANT.equals(cwc.getRoleType())).findFirst();
    final List<String> contactLines = new ArrayList<>();
    cwcOpt.ifPresent(cwc ->
      contactLines.addAll(
          cwc.getContacts().stream()
            .flatMap(c -> Stream.of(c.getName(), String.format("%s, %s", c.getEmail(), c.getPhone())))
            .collect(Collectors.toList())));
    return contactLines;
  }

  private String siteAddressLine(ApplicationJson application) {
    if (application.getLocations() == null || application.getLocations().isEmpty()) {
      return "";
    }
    final Map<Integer, FixedLocationJson> fixedLocationsById = fetchFixedLocations(application);
    return application.getLocations().stream().map(l -> locationAddress(l, fixedLocationsById))
        .collect(Collectors.joining(ADDRESS_LINE_SEPARATOR));
  }

  /*
   * If the application references any fixed locations, fetches all fixed
   * locations that match the application's kind and creates a lookup map.
   * Otherwise, just returns an empty map.
   */
  Map<Integer, FixedLocationJson> fetchFixedLocations(ApplicationJson applicationJson) {
    if (applicationJson.getLocations().stream().map(l -> l.getFixedLocationIds())
        .allMatch(flIds -> flIds == null || flIds.isEmpty())) {
      return Collections.emptyMap();
    } else {
      final ApplicationKind applicationKind = applicationJson.getKind();
      return locationService.getFixedLocationList().stream()
          .filter(fl -> fl.getApplicationKind() == applicationKind)
          .collect(Collectors.toMap(FixedLocationJson::getId, Function.identity()));
    }
  }

  /*
   * Generate a location address text. If the location refers to fixed
   * locations, create the string "[Fixed location name], [Segments]". Otherwise
   * use location's postal address.
   */
  private String locationAddress(LocationJson locationJson, Map<Integer, FixedLocationJson> fixedLocationsById) {
    if (locationJson.getFixedLocationIds() != null && !locationJson.getFixedLocationIds().isEmpty()) {
      return fixedLocationAddresses(locationJson.getFixedLocationIds(), fixedLocationsById);
    } else {
      return Optional.ofNullable(locationJson.getPostalAddress()).map(pa -> postalAddress(pa)).orElse(UNKNOWN_ADDRESS);
    }
  }

  /*
   * Given a list of fixed locations, return an address line
   */
  private String fixedLocationAddresses(List<Integer> fixedLocationIds,
      Map<Integer, FixedLocationJson> fixedLocationsById) {
    Map<String, List<FixedLocationJson>> grouped = fixedLocationIds.stream()
        .map(id -> fixedLocationsById.get(id))
        .filter(fl -> fl != null)
        .collect(Collectors.groupingBy(FixedLocationJson::getArea));
    if (grouped.isEmpty()) {
      return BAD_LOCATION.getArea();
    }
    return grouped.entrySet().stream().map(es -> addressLineFor(es.getValue()))
        .collect(Collectors.joining(ADDRESS_LINE_SEPARATOR));
  }


  // Generate a line like "Rautatientori, lohkot A, C, D".
  // all locations are from same area, there is always at least one location.
  private CharSequence addressLineFor(List<FixedLocationJson> locations) {
    // Start with area name (e.g., "Rautatientori"):
    StringBuilder line = new StringBuilder(locations.get(0).getArea());
    String firstSection = locations.get(0).getSection();
    if (locations.size() == 1) {
      // Only one section and could be nameless:
      if (firstSection != null) {
        line.append(", lohko " + firstSection);
      }
    } else {
      // Many sections, so they all have names
      line.append(", lohkot " + firstSection);
      for (int i = 1; i < locations.size(); ++i) {
        line.append(String.format(", %s", locations.get(i).getSection()));
      }
    }
    return line;
  }

  /*
   * Return address like "Mannerheimintie 3, 00100 Helsinki", skip null/empty
   * values
   */
  String postalAddress(PostalAddressJson a) {
    final String postalCodeAndCity = Arrays.asList(a.getPostalCode(), a.getCity()).stream()
        .filter(s -> s != null && !s.isEmpty())
        .collect(Collectors.joining(" "));
    return Arrays.asList(a.getStreetAddress(), postalCodeAndCity).stream()
        .filter(s -> s != null && !s.isEmpty())
        .collect(Collectors.joining(", "));
  }

  private String eventNature(EventNature nature) {
    switch (nature) {
    case CLOSED:
      return "Kutsuvierastilaisuus tai muu vastaava suljettu tapahtuma";
    case PUBLIC_FREE:
      return "Yleisölle pääsymaksuton tapahtuma";
    case PUBLIC_NONFREE:
      return "Yleisölle pääsymaksullinen tapahtuma";
    default:
      return "***TUNTEMATON TAPAHTUMALUONNE***";
    }
  }

  private int daysBetween(ZonedDateTime startDate, ZonedDateTime endDateExclusive) {
    if (startDate == null || endDateExclusive == null) {
      return 0;
    }
    return (int) (startDate.until(endDateExclusive, ChronoUnit.DAYS));
  }

  // Get the stylesheet name to use for given application.
  private String styleSheetName(ApplicationJson application) {
    /*
     * FIXME: only EVENT, SHORT_TERM_RENTAL, and CABLE_REPORT are supported. For
     * others, use "DUMMY"
     */
    if (application.getType() == ApplicationType.EVENT
        || application.getType() == ApplicationType.SHORT_TERM_RENTAL
        || application.getType() == ApplicationType.CABLE_REPORT) {
      return application.getType().name();
    }
    return "DUMMY";
  }
}

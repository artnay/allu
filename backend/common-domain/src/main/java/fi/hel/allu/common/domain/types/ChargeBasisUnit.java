package fi.hel.allu.common.domain.types;

/**
 * ChargeBasisUnit specifies a unit for charge basis
 */
public enum ChargeBasisUnit {
  PIECE, SQUARE_METER, PERCENT,
  HOUR, DAY, WEEK, MONTH, YEAR;
}

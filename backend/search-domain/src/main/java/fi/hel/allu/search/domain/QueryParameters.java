package fi.hel.allu.search.domain;

import java.util.List;

public class QueryParameters {
  private List<QueryParameter> queryParameters;

  public List<QueryParameter> getQueryParameters() {
    return queryParameters;
  }

  public void setQueryParameters(List<QueryParameter> queryParameters) {
    this.queryParameters = queryParameters;
  }

}

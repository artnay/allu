export class PageRequest {
  constructor(
    public readonly page?: number,
    public readonly size?: number) {}
}

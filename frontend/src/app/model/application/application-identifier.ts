export class ApplicationIdentifier {
  constructor(
    public id?: number,
    public applicationId?: string
  ) {}
}

export enum EventNature {
  PUBLIC_FREE,
  PUBLIC_NONFREE,
  CLOSED,
  PROMOTION
}

export enum ChargeBasisUnit {
  PIECE,
  SQUARE_METER,
  PERCENT,
  HOUR,
  DAY,
  WEEK,
  MONTH,
  YEAR
}

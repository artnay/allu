export enum SupervisionTaskStatusType {
  /**
   * Approved task.
   */
  APPROVED,
  /**
   * Rejected task.
   */
  REJECTED,
  /**
   * Task state is open.
   */
  OPEN
}

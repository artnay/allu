export class ApplicationTag {
  constructor(
    public type?: string,
    public addedBy?: number,
    public creationTime?: Date
  ) {}
}

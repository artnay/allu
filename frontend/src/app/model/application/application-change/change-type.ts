enum ChangeType {
  CREATED, // Application created
  STATUS_CHANGED, // Application status changed
  CONTENTS_CHANGED // Application contents changed
}

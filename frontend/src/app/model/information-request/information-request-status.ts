export enum InformationRequestStatus {
  DRAFT = 'DRAFT',
  OPEN = 'OPEN',
  RESPONSE_RECEIVED = 'RESPONSE_RECEIVED',
  CLOSED = 'CLOSED'
}

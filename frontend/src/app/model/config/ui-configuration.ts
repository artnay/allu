export class UiConfiguration {
  constructor(
    public production?: boolean,
    public oauth2AuthorizationEndpointUrl?: string,
    public versionNumber?: string
  ) {}
}

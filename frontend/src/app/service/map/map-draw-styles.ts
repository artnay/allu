export const styleByApplicationType = {
  EXCAVATION_ANNOUNCEMENT: {color: '#6db6ff', opacity: 1, fillOpacity: 0.4},
  AREA_RENTAL: {color: '#006ddb', opacity: 1, fillOpacity: 0.4},
  TEMPORARY_TRAFFIC_ARRANGEMENTS: {color: '#db6d00', opacity: 1, fillOpacity: 0.4},
  CABLE_REPORT: {color: '#009292', opacity: 1, fillOpacity: 0.4},
  PLACEMENT_CONTRACT: {color: '#ff6db6', opacity: 1, fillOpacity: 0.4},
  EVENT: {color: '#920000', opacity: 1, fillOpacity: 0.4},
  SHORT_TERM_RENTAL: {color: '#490092', opacity: 1, fillOpacity: 0.4},
  NOTE: {color: '#24ff24', opacity: 1, fillOpacity: 0.4}
};

export const pathStyle = {
  DEFAULT: {color: '#F23555', opacity: 1, fillOpacity: 0.4},
  DEFAULT_DRAW: {color: '#F23555', dashArray: '8,6', opacity: 1, fillOpacity: 0.4},
  DEFAULT_EDIT: {color: '#F23555', dashArray: '8,6', moveMarkers: false, opacity: 1, fillOpacity: 0.4}
};

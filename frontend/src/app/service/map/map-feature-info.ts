export interface MapFeatureInfo {
  id: number;
  applicationId?: string;
  type?: string;
  name?: string;
  startTime: string;
  endTime: string;
  applicant?: string;
}

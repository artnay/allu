export interface BackendApplicationFieldChange {
  fieldName: string;
  oldValue: string;
  newValue: string;
}

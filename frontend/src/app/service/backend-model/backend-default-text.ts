export interface BackendDefaultText {
  id: number;
  applicationType: string;
  textType: string;
  textValue: string;
}

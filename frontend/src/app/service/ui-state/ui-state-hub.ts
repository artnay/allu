import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {UIState} from './ui-state';
import {toast} from 'angular2-materialize';
import {ErrorInfo} from './error-info';
import {messageToReadable} from './error-type';

/**
 * Class for handling UIState changes and notify
 * listening components about them.
 *
 * All changes create a new state into ui-state stream.
 */
@Injectable()
export class UIStateHub {

  private uiState$: BehaviorSubject<UIState> = new BehaviorSubject(new UIState());
  private displayedMessage$: Subject<string> = new Subject<string>();

  constructor() {
    this.displayedMessage$.asObservable()
      .debounceTime(50)
      .distinctUntilChanged()
      .subscribe(message => toast(message, 4000));
  }

  /**
   * Observable which conveys latest state of UI.
   */
  public uiState = () => this.uiState$.asObservable();

  /**
   * For adding a notification messageToReadable
   */
  public addMessage(message: string) {
    const currentState: UIState = this.uiState$.getValue();
    this.uiState$.next(new UIState(message, currentState.error));
    return Observable.empty();
  }

  /**
   * For clearing current notification messageToReadable
   */
  public clearMessage(): void {
    const currentState: UIState = this.uiState$.getValue();
    this.uiState$.next(new UIState(undefined, currentState.error));
  }

  /**
   * For adding an error messageToReadable
   */
  public addError(error: ErrorInfo) {
    const currentState: UIState = this.uiState$.getValue();
    this.uiState$.next(new UIState(currentState.message, error));
    this.displayedMessage$.next(messageToReadable(error.type));
    return Observable.empty();
  }

  /**
   * For clearing current error messageToReadable
   */
  public clearError(): void {
    const currentState: UIState = this.uiState$.getValue();
    this.uiState$.next(new UIState(currentState.message, undefined));
  }

  public clear(): void {
    this.uiState$.next(new UIState());
  }
}

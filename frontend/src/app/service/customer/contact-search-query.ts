export interface ContactSearchQuery {
  name: string;
  active: boolean;
}

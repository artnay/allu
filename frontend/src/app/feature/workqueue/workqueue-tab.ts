export enum WorkQueueTab {
  OWN, // Omat
  COMMON, // Yhteiset
  WAITING // Odottaa
}

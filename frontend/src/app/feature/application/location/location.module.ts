import {NgModule, ModuleWithProviders} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MatCardModule} from '@angular/material';

import {LocationComponent} from './location.component';
import {AlluCommonModule} from '../../common/allu-common.module';
import {SearchBarModule} from '../../searchbar/searchbar.module';
import {MapModule} from '../../map/map.module';
import {ProgressBarModule} from '../progressbar/progressbar.module';
import {TypeModule} from '../type/type.module';
import {StoredLocationsComponent} from './stored-locations.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AlluCommonModule,
    MatCardModule,
    SearchBarModule,
    MapModule,
    ProgressBarModule,
    TypeModule
  ],
  declarations: [
    LocationComponent,
    StoredLocationsComponent
  ],
  providers: [
  ],
  exports: [
    StoredLocationsComponent
  ]
})
export class LocationModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LocationModule
    };
  }
}

import {Action, select, Store} from '@ngrx/store';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as fromApplication from '../reducers';
import {MetadataService} from '../../../service/meta/metadata.service';
import {from, Observable, of} from 'rxjs/index';
import {catchError, filter, map, switchMap, tap} from 'rxjs/internal/operators';
import {NumberUtil} from '../../../util/number.util';
import * as MetaAction from '../actions/application-meta-actions';
import {ApplicationMetaActionType} from '../actions/application-meta-actions';
import * as ApplicationAction from '../actions/application-actions';
import {Injectable} from '@angular/core';
import {ApplicationActionType} from '@feature/application/actions/application-actions';
import {ApplicationService} from '@service/application/application.service';
import {ApplicationStore} from '@service/application/application-store';
import {NotifyFailure, NotifySuccess} from '@feature/notification/actions/notification-actions';
import {withLatestExisting} from '@feature/common/with-latest-existing';
import {ClearCoordinates} from '@feature/map/actions/address-search-actions';
import {findTranslation} from '@util/translations';

@Injectable()
export class ApplicationEffects {
  constructor(private actions: Actions,
              private store: Store<fromApplication.State>,
              private applicationStore: ApplicationStore,
              private applicationService: ApplicationService,
              private metadataService: MetadataService) {}

  @Effect()
  load: Observable<Action> = this.actions.pipe(
    ofType<ApplicationAction.Load>(ApplicationActionType.Load),
    filter(action => NumberUtil.isDefined(action.payload)),
    switchMap(action => this.applicationService.get(action.payload).pipe(
      tap(application => this.applicationStore.setApplication(application)), // TODO: Remove when all api calls use ngrx
      map(application => new ApplicationAction.LoadSuccess(application)),
      catchError(error => from([
        new ApplicationAction.LoadFailed(error),
        new NotifyFailure(error)
      ]))
    ))
  );

  @Effect()
  loadMeta: Observable<Action> = this.actions.pipe(
    ofType<MetaAction.Load>(ApplicationMetaActionType.Load),
    withLatestExisting(this.store.pipe(select(fromApplication.getCurrentApplication))),
    switchMap(([action, application]) => this.metadataService.loadByApplicationType(application.type).pipe(
      map(meta => new MetaAction.LoadSuccess(meta)),
      catchError(error => of(new MetaAction.LoadFailed(error)))
    ))
  );

  @Effect()
  removeClientApplicationData: Observable<Action> = this.actions.pipe(
    ofType<ApplicationAction.RemoveClientApplicationData>(ApplicationActionType.RemoveClientApplicationData),
    withLatestExisting(this.store.pipe(select(fromApplication.getCurrentApplication))),
    switchMap(([action, application]) => this.applicationService.removeClientApplicationData(application.id).pipe(
      tap(app => this.applicationStore.setApplication(app)), // TODO: Remove when all api calls use ngrx
      map(app => new ApplicationAction.LoadSuccess(app)),
      catchError(error => from([
        new ApplicationAction.LoadFailed(error),
        new NotifyFailure(error)
      ]))
    ))
  );

  @Effect()
  changeOwner: Observable<Action> = this.actions.pipe(
    ofType<ApplicationAction.ChangeOwner>(ApplicationActionType.ChangeOwner),
    switchMap(action => this.applicationService.changeOwner(action.payload.ownerId, action.payload.applicationIds).pipe(
      switchMap(() => [
        new ApplicationAction.ChangeOwnerSuccess(),
        new NotifySuccess(findTranslation('workqueue.notifications.ownerChanged')),
      ]),
      catchError(error => of(new NotifyFailure(error)))
    ))
  );

  @Effect()
  removeOwner: Observable<Action> = this.actions.pipe(
    ofType<ApplicationAction.RemoveOwner>(ApplicationActionType.RemoveOwner),
    switchMap(action => this.applicationService.removeOwner(action.payload).pipe(
      switchMap(() => [
        new ApplicationAction.RemoveOwnerSuccess(),
        new NotifySuccess(findTranslation('workqueue.notifications.ownerRemoved'))
      ]),
      catchError(error => of(new NotifyFailure(error)))
    ))
  );

  @Effect()
  onApplicationLoad: Observable<Action> = this.actions.pipe(
    ofType<ApplicationAction.Load>(ApplicationActionType.Load),
    map(() => new ClearCoordinates())
  );
}

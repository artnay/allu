import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ChargeBasisEntryForm} from './charge-basis-entry.form';
import {ChargeBasisUnit} from '../../../../model/application/invoice/charge-basis-unit';
import {ChargeBasisEntry} from '../../../../model/application/invoice/charge-basis-entry';
import {NumberUtil} from '../../../../util/number.util';
import {ChargeBasisType, manualChargeBasisTypes} from '../../../../model/application/invoice/charge-basis-type';
import {Subject} from 'rxjs/Subject';

export const CHARGE_BASIS_ENTRY_MODAL_CONFIG = {width: '600PX', data: {}};

@Component({
  selector: 'charge-basis-entry-modal',
  templateUrl: './charge-basis-entry-modal.component.html',
  styleUrls: [
    './charge-basis-entry-modal.component.scss'
  ]
})
export class ChargeBasisEntryModalComponent implements OnInit, OnDestroy {
  chargeBasisEntryForm: FormGroup;
  chargeBasisTypes = manualChargeBasisTypes.map(type => ChargeBasisType[type]);

  typeCtrl: FormControl;

  private destroy = new Subject<boolean>();

  constructor(public dialogRef: MatDialogRef<ChargeBasisEntryModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ChargeBasisEntryModalData,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    const entry = this.data.entry || new ChargeBasisEntry(ChargeBasisType.DISCOUNT, ChargeBasisUnit.PIECE, 1);
    this.chargeBasisEntryForm = ChargeBasisEntryForm.formGroup(this.fb, entry);
    this.typeCtrl = <FormControl>this.chargeBasisEntryForm.get('type');

    this.typeCtrl.valueChanges
      .distinctUntilChanged()
      .takeUntil(this.destroy)
      .subscribe(type => this.typeChanges(type));

    this.chargeBasisEntryForm.valueChanges
      .takeUntil(this.destroy)
      .subscribe(entryForm => this.updateNetPrice(entryForm));
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

  onSubmit(): void {
    const entry = ChargeBasisEntryForm.toChargeBasisEntry(this.chargeBasisEntryForm.getRawValue());
    entry.manuallySet = true;
    this.dialogRef.close(entry);
  }

  cancel(): void {
    this.dialogRef.close(undefined);
  }

  private updateNetPrice(form: ChargeBasisEntryForm) {
    if (NumberUtil.isDefined(form.unitPrice) && NumberUtil.isDefined(form.quantity)) {
      this.chargeBasisEntryForm.patchValue({netPrice: form.unitPrice * form.quantity}, {emitEvent: false});
    } else {
      this.chargeBasisEntryForm.patchValue({netPrice: undefined}, {emitEvent: false});
    }
  }

  private typeChanges(typeName: string): void {
    this.chargeBasisEntryForm.get('unitPrice').setValidators([Validators.required]); // this might be disabled by discount component
    this.chargeBasisEntryForm.reset({type: typeName});
  }
}

export interface ChargeBasisEntryModalData {
  entry: ChargeBasisEntry;
}

import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlluCommonModule} from '../common/allu-common.module';
import {DecisionComponent} from './decision.component';
import {DecisionActionsComponent} from './decision-actions.component';
import {DecisionModalComponent} from './decision-modal.component';
import {ApplicationBasicInfoComponent} from '../decision/application.basic-info.component';
import {DecisionHub} from '../../service/decision/decision-hub';
import {DecisionService} from '../../service/decision/decision.service';
import {ProgressBarModule} from '../application/progressbar/progressbar.module';
import {DistributionModule} from '../application/distribution/distribution.module';
import {DecisionProposalModalComponent} from './proposal/decision-proposal-modal.component';
import {DecisionProposalComponent} from './proposal/decision-proposal.component';
import {AttachmentThumbnailsComponent} from './attachment-thumbnails.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    AlluCommonModule,
    ProgressBarModule,
    DistributionModule
  ],
  declarations: [
    DecisionComponent,
    DecisionActionsComponent,
    DecisionModalComponent,
    ApplicationBasicInfoComponent,
    DecisionProposalModalComponent,
    DecisionProposalComponent,
    AttachmentThumbnailsComponent
  ],
  providers: [
    DecisionHub,
    DecisionService
  ],
  entryComponents: [
    DecisionModalComponent,
    DecisionProposalModalComponent
  ]
})
export class DecisionModule {}

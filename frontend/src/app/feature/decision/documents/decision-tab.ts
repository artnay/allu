export enum DecisionTab {
  DECISION = 'DECISION',
  CONTRACT = 'CONTRACT',
  OPERATIONAL_CONDITION = 'OPERATIONAL_CONDITION',
  WORK_FINISHED = 'WORK_FINISHED',
  TERMINATION = 'TERMINATION'
}

import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material';

import {AlluCommonModule} from '../common/allu-common.module';
import {MapModule} from '../map/map.module';
import {SidebarModule} from '../sidebar/sidebar.module';
import {ProjectEditComponent} from './edit/project-edit.component';
import {projectRoutes} from './project.routing';
import {ProjectHub} from '../../service/project/project-hub';
import {ProjectService} from '../../service/project/project.service';
import {ProjectSearchComponent} from './search/project-search.component';
import {ProjectSummaryComponent} from './summary/project-summary.component';
import {ProjectComponent} from './project.component';
import {ProjectApplicationsComponent} from './applications/project-applications.component';
import {ProjectResolve} from './project-resolve';
import {ProjectProjectsComponent} from './projects/project-projects.component';
import {ProjectState} from '../../service/project/project-state';

@NgModule({
  imports: [
    AlluCommonModule,
    RouterModule.forChild(projectRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MapModule,
    SidebarModule
  ],
  declarations: [
    ProjectComponent,
    ProjectEditComponent,
    ProjectSearchComponent,
    ProjectSummaryComponent,
    ProjectApplicationsComponent,
    ProjectProjectsComponent
  ],
  providers: [
    ProjectHub,
    ProjectService,
    ProjectResolve,
    ProjectState
  ]
})
export class ProjectModule {}

